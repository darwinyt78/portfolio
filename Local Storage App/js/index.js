
function visitLink(path) {
    let click = localStorage.getItem(path);
    localStorage.setItem(path, Number(click)+1);
}

function viewResults() {
    const three = 3;    

    let ul = document.createElement('ul');
    document.querySelector('.container').appendChild(ul);
  
    for (let i = 1; i <= three; i++) {
        let li = document.createElement('li');
        
        li.innerHTML = `You visited Page${i} ${localStorage.getItem(`Page${i}`)} time(s)`;
        ul.appendChild(li);
    }
        
        localStorage.clear();
}
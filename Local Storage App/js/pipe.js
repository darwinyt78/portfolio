function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}
  
const pipe = (value, ...funcs) => {
	let f = [...funcs]
	try {
		for(let index = 0; index < f.length; index++) {
			let element = f[index];
				
			if (!isFunction(element)){
				throw Error(`Provided argument at position ${index} is not a function!`)
			}
			
		value = element(value)
	}
	return value
	
	} catch (error) {
		return error
	}	
} 
const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');
 const capitalize = (value) =>
	value
	.split(' ')
	.map((val) => val.charAt(0).toUpperCase() + val.slice(1))
	.join(' ');

const appendGreeting = (value) => ` Hello, ${value}!`;
  
const result = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, appendGreeting);
const error = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, '');

alert(error); // Provided argument at position 2 is not a function!
alert(result); // Hello, John Doe!
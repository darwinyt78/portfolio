const appRoot = document.getElementById('app-root');
const languages = externalService.getCountryListByLanguage()
const heading = document.createElement('div');
const div = document.createElement('div');
const p = document.createElement('p');

heading.innerHTML = '<h1>Countries Search</h1>';
appRoot.appendChild(heading);
heading.classList.add('header');
heading.appendChild(div);
div.appendChild(p);

div.innerHTML = `
<div class="inputs">
    <p>Please choose type of search: </p>
    <div class="inputsBtn">
      <input type="radio" class="checkbox" id="region" name="typeOfChoose">  
      <label for="region">By Region</label></br>
      <input type="radio" class="checkbox" id="languages" name="typeOfChoose"> 
      <label for="languages">By Language</label>
    </div>

</div>
<div class="select">
  <p>Please choose search query:</p>
  <select disabled id="select">
    <option selected="selected">select value</option>
  </select>
</div>
<table id="table" class="table">
</table>
`

div.classList.add('choose')

let regions = externalService.getRegionsList()

let select = document.getElementById('select')

document.getElementById('region').addEventListener('click',onClickRegions)
document.getElementById('languages').addEventListener('click',onClickLangs)

function onClickLangs() {
  select.removeAttribute('disabled')
  
  while (select.firstChild) {
    select.removeChild(select.firstChild)
  }
  
  const optionDef = document.createElement('option');
  
  optionDef.innerText = 'select value'
  
  select.appendChild(optionDef)
  
  externalService.getLanguagesList().forEach(element => {
    const option = document.createElement('option');
    option.text = element;
    select.appendChild(option);
  });
}

function onClickRegions() {
  select.removeAttribute('disabled')
  
  while (select.firstChild) {
    select.removeChild(select.firstChild)
  }
  
  const optionDef = document.createElement('option');
  
  optionDef.innerText = 'select value'
  
  select.appendChild(optionDef)
  
  externalService.getRegionsList().forEach(element => {
    const option = document.createElement('option');
    option.text = element;
    select.appendChild(option);
  });
}

const tableByLang = externalService.getCountryListByLanguage();

select.addEventListener('change', (event) => {
  let pathToOptionText = event.target.options[event.target.selectedIndex].text;
  if (document.getElementById('region').checked) {
    buildTable(externalService.getCountryListByRegion(`${pathToOptionText}`, table.innerHTML=''));
  } else if (document.getElementById('languages').checked) {
    buildTable(externalService.getCountryListByLanguage(`${pathToOptionText}`, table.innerHTML=''));
  }
});

function buildTable(tableByLang) {
  const table = document.getElementById('table')
  let row = `
              <tr>
                <th>Country name <span onclick="countryArrow()" id="country_arrow">&#8593;</span></th>
                <th>Capital</th>
                <th>World Region</th>
                <th>Languages</th>
                <th>Area <span onclick="countryArrow()" id="country_arrow">&#8593;</span></th>
                <th>Flag</th>
              </tr>`
  for (let i = 0; i < tableByLang.length; i++) {
      row += `<tr>
                <td class="country_td">${tableByLang[i].name}</td>
                <td class="capital_td">${tableByLang[i].capital}</td>
                <td class="region_td">${tableByLang[i].region}</td>
                <td class="language_td">${Object.values(tableByLang[i].languages).join(', ')}</td>
                <td class="area_td">${tableByLang[i].area}</td>
                <td class="img_td">
                <img src="${tableByLang[i].flagURL}" alt="flag"</td>
              </tr>
    `
    
  }
  table.innerHTML += row
}

let arrowCountry = '&#8593;';
function countryArrow() {
  const countryArrow = document.getElementById('country_arrow');
  
  if (arrowCountry === '&#8593;') {
    arrowCountry = '&#8595;';
  } else {
    arrowCountry = '&#8593;';
  }
  
  reverseArrText(document.getElementsByClassName('country_td'));
  reverseArrText(document.getElementsByClassName('capital_td'));
  reverseArrText(document.getElementsByClassName('region_td'));
  reverseArrText(document.getElementsByClassName('language_td'));
  reverseArrText(document.getElementsByClassName('area_td'));
  reverseImg(document.getElementsByClassName('img_td'));
  
  countryArrow.innerHTML = arrowCountry;
}

function reverseArrText(arr) {
  let reverse = [];
  for (let i = 0; i < arr.length; i++) {
    reverse[i] = arr[arr.length - i - 1].innerHTML;
  }
  for (let i = 0; i < arr.length; i++) {
    arr[i].innerHTML = reverse[i];
  }
}

function reverseImg (images) {
  let reverse = [];
  for (let i = 0; i < images.length; i++) {
    reverse[i] = images[images.length - i - 1].firstElementChild.getAttribute('src');
  }
  for (let i = 0; i < images.length; i++) {
    images[i].firstElementChild.src = reverse[i];
  }
}